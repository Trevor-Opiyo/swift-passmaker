import Foundation

func main() {
	var passOptions: [Character] = []

	header()

	if (include (charType:"upper case letters") == true) {
		passOptions += ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	}
	if (include (charType:"lower case letters") == true) {
		passOptions += ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
	}
	if (include (charType:"integers") == true) {
		passOptions += ["1","2","3","4","5","6","7","8","9","0"]
	}
	if (include (charType:"special characters") == true) {
		passOptions += ["-","_","+","=","@","#","$","%","^","&","*"]
	}

	for _ in 1..<getLen() {
		print(passOptions.randomElement()!, terminator: "")
	}
	
}

func header() {
	print("\n---------------------------------\n       Password Generator\n---------------------------------")
}

func include (charType: String) -> Bool {
	repeat {
		print("\nInclude \(charType) in the password? [Y/N]")
		let answer = readLine()
		switch answer {
			case "Yes","yes","YES","y","Y":
				return true
			case "No","no","NO","n","N":
				return false
			default:
				print("\nInvalid input. Please enter \'Y\' or \'N\'")
		}
	} while true
 }

func getLen () -> Int {
	var answer: Int
	print("\nEnter an integer length for the password")
	answer = Int(readLine() ?? "6") ?? 6
	print("\nThe password is: ")
	return answer
}

main()
